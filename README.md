# `nefertari-guards`

[![Build Status](https://travis-ci.org/brandicted/nefertari-guards.svg?branch=master)](https://travis-ci.org/brandicted/nefertari-guards)
[![Documentation](https://readthedocs.org/projects/nefertari-guards/badge/?version=stable)](http://nefertari-guards.readthedocs.org)

Advanced ACLs for [Nefertari](https://github.com/brandicted/nefertari).
