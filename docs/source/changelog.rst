Changelog
=========

* :release:`0.1.1 <2015-11-18>`
* :bug:`-` Fixed a 400 error returned by elasticsearch aggregation queries with 'auth = true'

* :release:`0.1.0 <2015-10-07>`
* :feature:`-` Initial release
